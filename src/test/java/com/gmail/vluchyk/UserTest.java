package com.gmail.vluchyk;

import com.gmail.vluchyk.exceptions.WrongLoginException;
import com.gmail.vluchyk.exceptions.WrongPasswordException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class UserTest {
    private static final String LOGIN_IS_NOT_SPECIFIED = "The login is not specified.";
    private static final String WRONG_LOGIN = "Wrong login: Login must be no more than 20 characters long and contain only Latin letters.";
    private static final String PASSWORD_IS_NOT_SPECIFIED = "The password is not specified.";
    private static final String WRONG_PASSWORD = "Wrong password: The password must contain Latin letters and numbers. Must be at least 1 letter and 1 number. The length of the password is from 6 to 25 characters.";
    private static final String WRONG_CONFIRMED_PASSWORD = "The confirmed password doesn't match the password.";
    private static final String CORRECT_LOGIN = "vluchyk";
    private static final String CORRECT_PASSWORD = "Qwerty1";

    @Test
    void loginNull() {
        //given
        String login = null;

        //when
        WrongLoginException result = Assertions.assertThrows(WrongLoginException.class, () -> new User(login, CORRECT_PASSWORD, CORRECT_PASSWORD));

        //then
        Assertions.assertThrows(WrongLoginException.class, () -> new User(login, CORRECT_PASSWORD, CORRECT_PASSWORD));
        Assertions.assertEquals(LOGIN_IS_NOT_SPECIFIED, result.getMessage());
    }

    @Test
    void loginEmpty() {
        //given
        String login = "";

        //when
        WrongLoginException result = Assertions.assertThrows(WrongLoginException.class, () -> new User(login, CORRECT_PASSWORD, CORRECT_PASSWORD));

        //then
        Assertions.assertThrows(WrongLoginException.class, () -> new User(login, CORRECT_PASSWORD, CORRECT_PASSWORD));
        Assertions.assertEquals(LOGIN_IS_NOT_SPECIFIED, result.getMessage());
    }

    @Test
    void loginLongLength() {
        //given
        String login = "qwertyuiopqwertyuiopq";

        //when
        WrongLoginException result = Assertions.assertThrows(WrongLoginException.class, () -> new User(login, CORRECT_PASSWORD, CORRECT_PASSWORD));

        //then
        Assertions.assertEquals(21, login.length());
        Assertions.assertThrows(WrongLoginException.class, () -> new User(login, CORRECT_PASSWORD, CORRECT_PASSWORD));
        Assertions.assertEquals(WRONG_LOGIN, result.getMessage());
    }

    @Test
    void loginWithUASymbol() {
        //given
        String login = "vluchїk";

        //when
        WrongLoginException result = Assertions.assertThrows(WrongLoginException.class, () -> new User(login, CORRECT_PASSWORD, CORRECT_PASSWORD));

        //then
        Assertions.assertThrows(WrongLoginException.class, () -> new User(login, CORRECT_PASSWORD, CORRECT_PASSWORD));
        Assertions.assertEquals(WRONG_LOGIN, result.getMessage());
    }

    @Test
    void loginWithDigit() {
        //given
        String login = "vluсhyk5";

        //when
        WrongLoginException result = Assertions.assertThrows(WrongLoginException.class, () -> new User(login, CORRECT_PASSWORD, CORRECT_PASSWORD));

        //then
        Assertions.assertThrows(WrongLoginException.class, () -> new User(login, CORRECT_PASSWORD, CORRECT_PASSWORD));
        Assertions.assertEquals(WRONG_LOGIN, result.getMessage());
    }

    @Test
    void loginWithDot() {
        //given
        String login = "v.luсhyk";

        //when
        WrongLoginException result = Assertions.assertThrows(WrongLoginException.class, () -> new User(login, CORRECT_PASSWORD, CORRECT_PASSWORD));

        //then
        Assertions.assertThrows(WrongLoginException.class, () -> new User(login, CORRECT_PASSWORD, CORRECT_PASSWORD));
        Assertions.assertEquals(WRONG_LOGIN, result.getMessage());
    }

    @Test
    void passwordNull() {
        //given
        String password = null;

        //when
        WrongPasswordException result = Assertions.assertThrows(WrongPasswordException.class, () -> new User(CORRECT_LOGIN, password, password));

        //then
        Assertions.assertThrows(WrongPasswordException.class, () -> new User(CORRECT_LOGIN, password, password));
        Assertions.assertEquals(PASSWORD_IS_NOT_SPECIFIED, result.getMessage());
    }

    @Test
    void passwordEmpty() {
        //given
        String password = "";

        //when
        WrongPasswordException result = Assertions.assertThrows(WrongPasswordException.class, () -> new User(CORRECT_LOGIN, password, password));

        //then
        Assertions.assertThrows(WrongPasswordException.class, () -> new User(CORRECT_LOGIN, password, password));
        Assertions.assertEquals(PASSWORD_IS_NOT_SPECIFIED, result.getMessage());
    }

    @Test
    void passwordJustLatinLetters() {
        //given
        String password = "Qwerty";

        //when
        WrongPasswordException result = Assertions.assertThrows(WrongPasswordException.class, () -> new User(CORRECT_LOGIN, password, password));

        //then
        Assertions.assertThrows(WrongPasswordException.class, () -> new User(CORRECT_LOGIN, password, password));
        Assertions.assertEquals(WRONG_PASSWORD, result.getMessage());
    }

    @Test
    void passwordJustDigits() {
        //given
        String password = "123456";

        //when
        WrongPasswordException result = Assertions.assertThrows(WrongPasswordException.class, () -> new User(CORRECT_LOGIN, password, password));

        //when + then
        Assertions.assertThrows(WrongPasswordException.class, () -> new User(CORRECT_LOGIN, password, password));
        Assertions.assertEquals(WRONG_PASSWORD, result.getMessage());
    }

    @Test
    void passwordWithUASymbol() {
        //given
        String password = "Qwertї123";

        //when
        WrongPasswordException result = Assertions.assertThrows(WrongPasswordException.class, () -> new User(CORRECT_LOGIN, password, password));

        //then
        Assertions.assertThrows(WrongPasswordException.class, () -> new User(CORRECT_LOGIN, password, password));
        Assertions.assertEquals(WRONG_PASSWORD, result.getMessage());
    }

    @Test
    void passwordShortLength() {
        //given
        String password = "Qwe12";

        //when
        WrongPasswordException result = Assertions.assertThrows(WrongPasswordException.class, () -> new User(CORRECT_LOGIN, password, password));

        //then
        Assertions.assertEquals(5, password.length());
        Assertions.assertThrows(WrongPasswordException.class, () -> new User(CORRECT_LOGIN, password, password));
        Assertions.assertEquals(WRONG_PASSWORD, result.getMessage());
    }

    @Test
    void passwordLongLength() {
        //given
        String password = "Qwerty1234Qwerty1234Qwerty";

        //when
        WrongPasswordException result = Assertions.assertThrows(WrongPasswordException.class, () -> new User(CORRECT_LOGIN, password, password));

        //then
        Assertions.assertEquals(26, password.length());
        Assertions.assertThrows(WrongPasswordException.class, () -> new User(CORRECT_LOGIN, password, password));
        Assertions.assertEquals(WRONG_PASSWORD, result.getMessage());
    }

    @Test
    void passwordDot() {
        //given
        String password = "Qwerty.1234";

        //when
        WrongPasswordException result = Assertions.assertThrows(WrongPasswordException.class, () -> new User(CORRECT_LOGIN, password, password));

        //then
        Assertions.assertThrows(WrongPasswordException.class, () -> new User(CORRECT_LOGIN, password, password));
        Assertions.assertEquals(WRONG_PASSWORD, result.getMessage());
    }

    @Test
    void confirmedPasswordDoesNotMatchPassword() {
        //given
        String confirmedPassword = "qwerty1";

        //when
        WrongPasswordException result = Assertions.assertThrows(WrongPasswordException.class, () -> new User(CORRECT_LOGIN, CORRECT_PASSWORD, confirmedPassword));

        //then
        Assertions.assertThrows(WrongPasswordException.class, () -> new User(CORRECT_LOGIN, CORRECT_PASSWORD, confirmedPassword));
        Assertions.assertEquals(WRONG_CONFIRMED_PASSWORD, result.getMessage());
    }

    @Test
    void createUserSuccess() {
        //when
        User user = new User(CORRECT_LOGIN, CORRECT_PASSWORD, CORRECT_PASSWORD);

        //then
        Assertions.assertEquals(CORRECT_LOGIN, user.getLogin());
        Assertions.assertEquals(CORRECT_PASSWORD, user.getPassword());
    }
}