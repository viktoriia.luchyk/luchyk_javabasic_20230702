package com.gmail.vluchyk;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PointTest {
    private Point firstPoint;

    @BeforeEach
    void setUp() {
        firstPoint = new Point("A", 1, 2);
    }

    @Test
    public void anotherPoint() {
        //when
        Point anotherPoint = new Point(firstPoint);

        //then
        Assertions.assertEquals(firstPoint, anotherPoint);
    }

    @Test
    public void pointWithNullName() {
        //when
        Point point = new Point(null, 7, 8);
        String name = point.getName();

        //then
        Assertions.assertEquals("", name);
    }

    @Test
    public void anotherPointWithNullName() {
        //given
        Point point = new Point(null, 7, 8);

        //when
        Point anotherPoint = new Point(point);
        String name = anotherPoint.getName();

        //then
        Assertions.assertEquals("", name);
    }

    @Test
    public void checkGetName() {
        //when
        String name = firstPoint.getName();

        //then
        Assertions.assertEquals("A", name);
    }

    @Test
    public void checkSetName() {
        //given
        Point anotherPoint = new Point(firstPoint);

        //when
        anotherPoint.setName("B");
        String name = anotherPoint.getName();

        //then
        Assertions.assertEquals("B", name);
    }

    @Test
    public void checkSetNullName() {
        //given
        Point point = new Point("A", 7, 8);

        //when
        point.setName(null);
        String name = point.getName();

        //then
        Assertions.assertEquals("", name);
    }

    @Test
    public void checkGetX() {
        //when
        int x = firstPoint.getX();

        //then
        Assertions.assertEquals(1, x);
    }

    @Test
    public void checkSetX() {
        //given
        Point anotherPoint = new Point(firstPoint);

        //when
        anotherPoint.setX(5);
        int x = anotherPoint.getX();

        //then
        Assertions.assertEquals(5, x);
    }

    @Test
    public void checkGetY() {
        //when
        int y = firstPoint.getY();

        //then
        Assertions.assertEquals(2, y);
    }

    @Test
    public void checkSetY() {
        //given
        Point anotherPoint = new Point(firstPoint);

        //when
        anotherPoint.setY(5);
        int y = anotherPoint.getY();

        //then
        Assertions.assertEquals(5, y);
    }

    @Test
    public void distanceToPoint() {
        //given
        Point secondPoint = new Point("B", 3, 4);

        //when
        double distance = firstPoint.distance(secondPoint);

        //then
        Assertions.assertEquals(2.8284, distance, 0.0001);
    }

    @Test
    public void distanceToNullPoint() {
        //given
        Point secondPoint = null;

        //when
        double distance = firstPoint.distance(secondPoint);

        //then
        Assertions.assertEquals(0, distance);
    }

    @Test
    public void distanceBetweenTwoPoints() {
        //given
        Point secondPoint = new Point("B", -3, -4);

        //when
        double distance = Point.distance(firstPoint, secondPoint);

        //then
        Assertions.assertEquals(7.2111, distance, 0.0001);
    }

    @Test
    public void distanceBetweenTwoPointsFirstNull() {
        //given
        Point point = null;
        Point secondPoint = new Point("B", -3, -4);

        //when
        double distance = Point.distance(point, secondPoint);

        //then
        Assertions.assertEquals(0, distance);
    }

    @Test
    public void distanceBetweenTwoPointsSecondNull() {
        //given
        Point secondPoint = null;

        //when
        double distance = Point.distance(firstPoint, secondPoint);

        //then
        Assertions.assertEquals(0, distance);
    }

    @Test
    public void checkNullPoint() {
        //given
        Point point = null;

        //when
        boolean result = Point.check(point);

        //then
        Assertions.assertFalse(result);
    }

    @Test
    public void checkPoint() {
        //when
        boolean result = Point.check(firstPoint);

        //then
        Assertions.assertTrue(result);
    }

    @Test
    public void equalsReflexive() {
        //when
        boolean result = firstPoint.equals(firstPoint);

        //then
        Assertions.assertTrue(result);
    }

    @Test
    public void equalsSymmetric() {
        //given
        Point secondPoint = new Point("B", 1, 2);

        //when
        boolean firstResult = firstPoint.equals(secondPoint);
        boolean secondResult = secondPoint.equals(firstPoint);

        //then
        Assertions.assertTrue(firstResult);
        Assertions.assertEquals(firstResult, secondResult);

    }

    @Test
    public void equalsTransitive() {
        //given
        Point secondPoint = new Point("B", 1, 2);
        Point thirdPoint = new Point("C", 1, 2);

        //when
        boolean firstResult = firstPoint.equals(secondPoint);
        boolean secondResult = secondPoint.equals(thirdPoint);
        boolean thirdResult = firstPoint.equals(thirdPoint);

        //then
        Assertions.assertTrue(firstResult);
        Assertions.assertTrue(secondResult);
        Assertions.assertEquals(firstResult, thirdResult);
    }

    @Test
    public void equalsConsistentTrue() {
        //given
        Point secondPoint = new Point("B", 1, 2);
        int numberOfResults = 100;

        //when
        boolean result = true;
        for (int i = 0; i < numberOfResults; i++) {
            result = firstPoint.equals(secondPoint) && result;
        }

        //then
        Assertions.assertTrue(result);
    }

    @Test
    public void equalsConsistentFalse() {
        //given
        Point secondPoint = new Point("B", 1, 3);
        int numberOfResults = 100;

        //when
        boolean result = false;
        for (int i = 0; i < numberOfResults; i++) {
            result = firstPoint.equals(secondPoint) || result;
        }

        //then
        Assertions.assertFalse(result);

    }

    @Test
    public void equalsToNull() {
        //given
        Point point = null;

        //when
        boolean result = firstPoint.equals(point);

        //then
        Assertions.assertFalse(result);
    }

    @Test
    public void equalsDifferentObjects() {
        //given
        Person person = new Person("John", "Smith", "London", "+447949588237");

        //when
        boolean result = firstPoint.equals(person);

        //then
        Assertions.assertFalse(result);
    }

    @Test
    public void hashCodeConsistent() {
        //given
        int numberOfResults = 100;

        //when
        boolean result = true;
        int valueHashCode = firstPoint.hashCode();
        for (int i = 0; i < numberOfResults; i++) {
            result = (firstPoint.hashCode() == valueHashCode && result);
        }

        //then
        Assertions.assertTrue(result);
    }

    @Test
    public void hashCodeOfEqualObjects() {
        //given
        Point secondPoint = new Point("B", 1, 2);

        //when
        boolean result = (firstPoint.equals(secondPoint) && firstPoint.hashCode() == secondPoint.hashCode());

        //then
        Assertions.assertTrue(result);
    }

    @Test
    public void checkClone() throws CloneNotSupportedException {
        //when
        Point clonedPoint = firstPoint.clone();

        //then
        Assertions.assertEquals(firstPoint, clonedPoint);
    }
}