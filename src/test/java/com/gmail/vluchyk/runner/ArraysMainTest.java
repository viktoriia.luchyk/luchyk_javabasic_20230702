package com.gmail.vluchyk.runner;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ThreadLocalRandom;

class ArraysMainTest {
    @Test
    void checkMatrixSquareCase() {
        //given
        int size = 25;
        double[][] matrix = new double[size][size];

        //when
        boolean result = ArraysMain.isMatrixSquare(matrix);

        //then
        Assertions.assertTrue(result);
    }

    @Test
    void checkMatrixNonSquareCase() {
        //given
        int height = 10;
        int width = 15;
        double[][] matrix = new double[height][width];

        //when
        boolean result = ArraysMain.isMatrixSquare(matrix);

        //then
        Assertions.assertFalse(result);
    }

    @Test
    void checkMatrixStepArraysCase() {
        //given
        int height = 30;
        int maxWidth = 15;
        double[][] matrix = new double[height][];
        for (int i = 0; i < matrix.length; i++) {
            matrix[i] = new double[ThreadLocalRandom.current().nextInt(0, maxWidth + 1)];
        }

        //when
        boolean result = ArraysMain.isMatrixSquare(matrix);

        //then
        Assertions.assertFalse(result);
    }

    @Test
    void checkMatrixNullCase() {
        //when
        boolean result = ArraysMain.isMatrixSquare(null);

        //then
        Assertions.assertFalse(result);
    }

    @Test
    void checkMatrixZeroHeightCase() {
        //given
        double[][] matrix = new double[0][];

        //when
        boolean result = ArraysMain.isMatrixSquare(matrix);

        //then
        Assertions.assertFalse(result);
    }

    @Test
    void checkMatrixWithNullArrayCase() {
        //given
        double[][] matrix = {{1, 2, 3}, null, {4, 5, 6}};

        //when
        boolean result = ArraysMain.isMatrixSquare(matrix);

        //then
        Assertions.assertFalse(result);
    }

    @Test
    void checkMatrixJustOneNullArrayCase() {
        //given
        double[][] matrix = {null};

        //when
        boolean result = ArraysMain.isMatrixSquare(matrix);

        //then
        Assertions.assertFalse(result);
    }

    @Test
    void generateArrayZeroSizeCase() {
        //given
        int size = 0;

        //when
        int length = ArraysMain.generateArray(size).length;

        //then
        Assertions.assertEquals(0, length);
    }

    @Test
    void generateArrayNegativeSizeCase() {
        //given
        int size = -5;

        //when
        int length = ArraysMain.generateArray(size).length;

        //then
        Assertions.assertEquals(0, length);
    }

    @Test
    void generateArrayPositiveSizeCase() {
        //given
        int size = 10;

        //when
        int length = ArraysMain.generateArray(size).length;

        //then
        Assertions.assertEquals(10, length);
    }

    @Test
    void averageOfArrayNullCase() {
        //when
        double value = ArraysMain.average(null);

        //then
        Assertions.assertEquals(0, value, 0.0001);
    }

    @Test
    void averageOfArrayZeroLengthCase() {
        //given
        double[] array = new double[0];

        //when
        double value = ArraysMain.average(array);

        //then
        Assertions.assertEquals(0, value, 0.0001);
    }

    @Test
    void averageOfArray() {
        //given
        double[] array = {1.15, 2.25, -3.35, 4.45, 5.55};

        //when
        double value = ArraysMain.average(array);

        //then
        Assertions.assertEquals(2.01, value, 0.0001);
    }
}