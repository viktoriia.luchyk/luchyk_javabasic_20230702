package com.gmail.vluchyk;

public class CounterStrike {
    public static void main(String[] args) {
        CSTeam team1 = new CSTeam("", 0, 0, 0, 0, 0);
        System.out.println("Specify the information of the first team.");
        team1 = team1.createTeamInfo();
        CSTeam team2 = new CSTeam("", 0, 0, 0, 0, 0);
        System.out.println("Specify the information of the second team.");
        team2 = team2.createTeamInfo();

        System.out.println("The following information were entered:");
        team1.printTeamInfo();
        team2.printTeamInfo();

        double team1AverageFrag = team1.getTeamAverageFrag();
        double team2AverageFrag = team2.getTeamAverageFrag();

        System.out.println("The following information were calculated:");
        team1.printTeamAverageFragInfo(team1AverageFrag);
        team2.printTeamAverageFragInfo(team2AverageFrag);

        System.out.println("RESULT:");
        double threshold = 0.001;
        if (Math.abs(team1AverageFrag - team2AverageFrag) < threshold) {
            System.out.println("The teams scored the same number of points.");
        } else if (team1AverageFrag > team2AverageFrag) {
            System.out.printf("The winner is team %s, scored %.1f points.", team1.getName(), team1AverageFrag);
        } else {
            System.out.printf("The winner is team %s, scored %.1f points.", team2.getName(), team2AverageFrag);
        }
    }
}