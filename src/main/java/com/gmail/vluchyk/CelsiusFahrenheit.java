package com.gmail.vluchyk;

public class CelsiusFahrenheit extends AbstractConverter {
    private double celsius;
    private double fahrenheit;

    public CelsiusFahrenheit() {
    }

    public double getCelsius() {
        return celsius;
    }

    public void setCelsius(double celsius) {
        this.celsius = celsius;
        convertFromCelsius();
    }

    public double getFahrenheit() {
        return fahrenheit;
    }

    public void setFahrenheit(double fahrenheit) {
        this.fahrenheit = fahrenheit;
        convertToCelsius();
    }

    @Override
    protected void convertFromCelsius() {
        this.fahrenheit = this.celsius * 1.8 + 32;
    }

    @Override
    protected void convertToCelsius() {
        this.celsius = (this.fahrenheit - 32) / 1.8;
    }
}