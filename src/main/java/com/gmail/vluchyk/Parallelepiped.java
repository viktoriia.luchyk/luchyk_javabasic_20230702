package com.gmail.vluchyk;

import java.util.Scanner;

public class Parallelepiped {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the width (cm) of the parallelepiped:");
        float objectWidth = scanner.nextFloat();
        while (objectWidth <= 0) {
            System.out.println("Incorrect. The value must be greater than 0.");
            System.out.println("Enter the width (cm) of the parallelepiped:");
            objectWidth = scanner.nextFloat();
        }

        System.out.println("Enter the length (cm) of the parallelepiped:");
        float objectLength = scanner.nextFloat();
        while (objectLength <= 0) {
            System.out.println("Incorrect. The value must be greater than 0.");
            System.out.println("Enter the length (cm) of the parallelepiped:");
            objectLength = scanner.nextFloat();
        }

        System.out.println("Enter the height (cm) of the parallelepiped:");
        float objectHeight = scanner.nextFloat();
        while (objectHeight <= 0) {
            System.out.println("Incorrect. The value must be greater than 0.");
            System.out.println("Enter the height (cm) of the parallelepiped:");
            objectHeight = scanner.nextFloat();
        }

        System.out.println("The following values were specified:");
        System.out.println("Width = " + objectWidth + ", Length = " + objectLength + ", Height = " + objectHeight);

        double volume = Math.round(objectWidth * objectLength * objectHeight * 10) / 10.0;
        System.out.println("Volume of the parallelepiped = " + volume + " cubic cm");

        double length = Math.round((objectWidth + objectLength + objectHeight) * 4 * 10) / 10.0;
//        double length = (objectWidth + objectLength + objectHeight) * 4;
        System.out.println("Perimeter of the parallelepiped = " + length + " cm");

        scanner.close();
    }
}
