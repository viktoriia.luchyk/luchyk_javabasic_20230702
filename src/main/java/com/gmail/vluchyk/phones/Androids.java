package com.gmail.vluchyk.phones;

import com.gmail.vluchyk.interfaces.LinuxOS;
import com.gmail.vluchyk.interfaces.Smartphones;

public class Androids implements Smartphones, LinuxOS {
    private String phoneNumber = "";

    public Androids(String phoneNumber) {
        if (isFilled(phoneNumber)) {
            this.phoneNumber = phoneNumber;
        }
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        if (isFilled(phoneNumber)) {
            this.phoneNumber = phoneNumber;
        }
    }

    @Override
    public void call() {
        System.out.print("The call is from Android. ");
        printPhoneNumber();
        System.out.println();
    }

    @Override
    public void sms() {
        System.out.print("The sms is from Android. ");
        printPhoneNumber();
        System.out.println();
    }

    @Override
    public void internet() {
        System.out.print("The internet is connected to Android. ");
        printPhoneNumber();
        System.out.println();
    }

    @Override
    public void installOS() {
        System.out.println("LinuxOS is successfully installed on your phone.");
    }

    @Override
    public void updateOS() {
        System.out.println("The new version of LinuxOS is successfully installed on your phone.");
    }

    @Override
    public void installThirdPartyApp() {
        System.out.println("The third party application is installed on your phone.");
    }

    @Override
    public void useMemoryCard() {
        System.out.println("The memory card is used.");
    }

    private boolean isFilled(String field) {
        return field != null && !field.equals("");
    }

    private void printPhoneNumber() {
        if (isFilled(this.phoneNumber)) {
            System.out.print("The phone number is " + this.phoneNumber + ".");
        }
    }
}
