package com.gmail.vluchyk.phones;

import com.gmail.vluchyk.interfaces.IOS;
import com.gmail.vluchyk.interfaces.Smartphones;

public class IPhones implements Smartphones, IOS {
    private String owner = "";

    public IPhones(String owner) {
        if (isFilled(owner)) {
            this.owner = owner;
        }
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        if (isFilled(owner)) {
            this.owner = owner;
        }
    }

    @Override
    public void call() {
        System.out.print("The call is from iPhone. ");
        printOwner();
        System.out.println();
    }

    @Override
    public void sms() {
        System.out.print("The sms is from iPhone. ");
        printOwner();
        System.out.println();
    }

    @Override
    public void internet() {
        System.out.print("The internet is connected to iPhone. ");
        printOwner();
        System.out.println();
    }

    @Override
    public void installOS() {
        System.out.println("iOS is successfully installed on your phone.");
    }

    @Override
    public void updateOS() {
        System.out.println("The new version of iOS is successfully installed on your phone.");
    }

    @Override
    public void airDrop() {
        System.out.println("The file is transferred between your devices.");
    }

    @Override
    public void synchronize() {
        System.out.println("Your devices are synchronized.");
    }

    private boolean isFilled(String field) {
        return field != null && !field.equals("");
    }

    private void printOwner() {
        if (isFilled(this.owner)) {
            System.out.print("The owner is " + this.owner + ".");
        }
    }
}
