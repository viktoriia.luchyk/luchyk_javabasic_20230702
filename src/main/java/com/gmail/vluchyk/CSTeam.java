package com.gmail.vluchyk;

import java.util.Scanner;

public class CSTeam {
    private String name;
    private int frag1;
    private int frag2;
    private int frag3;
    private int frag4;
    private int frag5;

    public CSTeam(String name, int frag1, int frag2, int frag3, int frag4, int frag5) {
        this.name = name;
        this.frag1 = frag1;
        this.frag2 = frag2;
        this.frag3 = frag3;
        this.frag4 = frag4;
        this.frag5 = frag5;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFrag1() {
        return frag1;
    }

    public void setFrag1(int frag1) {
        this.frag1 = frag1;
    }

    public int getFrag2() {
        return frag2;
    }

    public void setFrag2(int frag2) {
        this.frag2 = frag2;
    }

    public int getFrag3() {
        return frag3;
    }

    public void setFrag3(int frag3) {
        this.frag3 = frag3;
    }

    public int getFrag4() {
        return frag4;
    }

    public void setFrag4(int frag4) {
        this.frag4 = frag4;
    }

    public int getFrag5() {
        return frag5;
    }

    public void setFrag5(int frag5) {
        this.frag5 = frag5;
    }

    public CSTeam createTeamInfo() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Name:");
        String teamName = scanner.nextLine();
        System.out.println("Frag 1:");
        int teamFrag1 = scanner.nextInt();
        System.out.println("Frag 2:");
        int teamFrag2 = scanner.nextInt();
        System.out.println("Frag 3:");
        int teamFrag3 = scanner.nextInt();
        System.out.println("Frag 4:");
        int teamFrag4 = scanner.nextInt();
        System.out.println("Frag 5:");
        int teamFrag5 = scanner.nextInt();

        CSTeam team = new CSTeam(teamName, teamFrag1, teamFrag2, teamFrag3, teamFrag4, teamFrag5);

        return team;
    }

    public void printTeamInfo() {
        System.out.printf("Team %s, Frags: %,d, %,d, %,d, %,d, %,d.\n", this.name, this.frag1, this.frag2, this.frag3, this.frag4, this.frag5);
    }

    public double getTeamAverageFrag() {
        return (double) (this.frag1 + this.frag2 + this.frag3 + this.frag4 + this.frag5) / 5;
    }

    public void printTeamAverageFragInfo(double teamAverageFrag) {
        System.out.printf("Team %s, Average Frag = %.1f.\n", this.name, teamAverageFrag);
    }
}
