package com.gmail.vluchyk.matrix;

import com.gmail.vluchyk.array.MyIntArray;

import java.util.concurrent.ThreadLocalRandom;

public class MyIntMatrix {
    private MyIntArray[] matrix;
    private int height;
    private int grandTotal;

    public MyIntMatrix(int height, int maxWidth) {
        this.matrix = new MyIntArray[height];
        this.height = height;
        fillMatrix(maxWidth);
        calculateGrandTotal();
    }

    public MyIntArray[] getMatrix() {
        return matrix;
    }

    public void setMatrix(MyIntArray[] matrix) {
        if (this.matrix.length == matrix.length) {
            for (int i = 0; i < this.matrix.length; i++) {
                this.matrix[i] = matrix[i];
            }
        }
        this.height = matrix.length;
        calculateGrandTotal();
    }

    public int getHeight() {
        return height;
    }

    public int getGrandTotal() {
        return grandTotal;
    }

    private void calculateGrandTotal() {
        this.grandTotal = 0;
        for (int i = 0; i < this.matrix.length; i++) {
            grandTotal += this.matrix[i].getTotalSum();
        }
    }

    private void fillMatrix(int maxWidth) {
        for (int i = 0; i < this.matrix.length; i++) {
            this.matrix[i] = new MyIntArray(ThreadLocalRandom.current().nextInt(0, maxWidth));
        }
    }

    public void sortByRows() {
        for (int i = 0; i < this.matrix.length; i++) {
            MyIntArray row = this.matrix[i];
            if (i % 2 > 0) {
                row.quickSort(row.getArray(), 0, this.matrix[i].getSize() - 1);
            } else {
                row.quickSortReverse(row.getArray(), 0, this.matrix[i].getSize() - 1);
            }
            this.matrix[i] = row;
        }
    }

    public int[] findMinElements() {
        int[] minArray = new int[this.matrix.length];
        for (int i = 0; i < this.matrix.length; i++) {
            if (this.matrix[i].getSize() > 0) {
                minArray[i] = this.matrix[i].getMinElement();
            }
        }
        return minArray;
    }

    public void divide(int divider) {
        if (divider != 0) {
            for (int i = 0; i < this.matrix.length; i++) {
                MyIntArray row = this.matrix[i];
                row.divide(divider);
                this.matrix[i] = row;
            }
        } else {
            System.out.println("\nThe matrix is unchanged because it cannot be divided by 0.");
        }
    }

    public Integer[][] convertToInteger() {
        Integer[][] convertedMatrix = new Integer[this.matrix.length][];
        for (int i = 0; i < this.matrix.length; i++) {
            convertedMatrix[i] = this.matrix[i].convertToInteger();
        }
        return convertedMatrix;
    }

    public void print() {
        /*
        for (int i = 0; i < this.matrix.length; i++) {
            this.matrix[i].print();
        }
         */
        System.out.println("[");
        for (int i = 0; i < this.matrix.length; i++) {
            System.out.print("\t");
            this.matrix[i].print();
        }
        System.out.println("]");
    }

    public void printInfo(String name) {
        System.out.printf(name + " (%d x M*):\n", this.matrix.length);
    }
}
