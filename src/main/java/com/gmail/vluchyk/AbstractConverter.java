package com.gmail.vluchyk;

public abstract class AbstractConverter {
    protected abstract void convertFromCelsius();

    protected abstract void convertToCelsius();
}