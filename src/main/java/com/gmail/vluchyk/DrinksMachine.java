package com.gmail.vluchyk;

import java.util.Scanner;

public class DrinksMachine {
    private static Scanner scanner = new Scanner(System.in);
    private static double priceTotal;
    private static int count;
    private static final String YES = "Y";

    public static void main(String[] args) {
        printMenu();
        makeOrder();
        System.out.printf("Please pay %.2f for %d drink%s.", priceTotal, count, (count > 1 ? "s" : ""));
        scanner.close();
    }

    public static void printMenu() {
        System.out.println("Available drinks are: ");
        Drink[] drinks = Drink.values();
        for (int i = 0; i < drinks.length; i++) {
            System.out.printf("\t%s (%.2f UAH)", drinks[i].getName(), drinks[i].getPrice());
            if (i < drinks.length - 1) {
                System.out.print(",\n");
            }
        }
        System.out.println("\n");
    }

    public static Drink choice() {
        String value;
        String answer;
        do {
            value = scanner.nextLine();
            for (Drink drink : Drink.values()) {
                if (drink.getName().equalsIgnoreCase(value)) {
                    return drink;
                }
            }
            System.out.println("Incorrect input. Try again? Y/N");
            answer = scanner.nextLine();
        } while (YES.equalsIgnoreCase(answer));
        return null;
    }

    private static void makeOrder() {
        String answer;
        do {
            System.out.println("Please make your choice.");
            Drink drink = choice();
            if (drink != null) {
                drink.run();
                priceTotal += drink.getPrice();
                count++;
            }
            System.out.println("One more drink? Y/N");
            answer = scanner.nextLine();
        } while (YES.equalsIgnoreCase(answer));
    }
}

enum Drink implements Runnable {
    COFFEE("Coffee", 50),
    TEA("Tea", 35),
    LEMONADE("Lemonade", 45),
    MOJITO("Mojito", 63),
    MINERAL_WATER("Mineral Water", 20),
    COCA_COLA("Coca-Cola", 25);

    private String name;
    private double price;

    Drink(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    private void prepareCoffee() {
        System.out.println("10-24 g of coffee + hot water (T = 88-92°C) + 15 bar in a coffee machine.");
    }

    private void prepareTea() {
        System.out.println("2 g of green tea + hot water (T = 85°C).");
    }

    private void prepareLemonade() {
        System.out.println("300 g of sugar + 2 oranges + 1 lemon + 4 liters of water.");
    }

    private void prepareMojito() {
        System.out.println("4-6 leaves of mint + 3 tablespoons of sugar + 1 half of lime (lemon) + 30 ml of vodka + 60 ml of soda + 100 g of ice cubes.");
    }

    private void prepareMineralWater() {
        System.out.println("Just mineral water.");
    }

    private void prepareCocaCola() {
        System.out.println("Coca-Cola + 100 g of ice cubes.");
    }

    @Override
    public void run() {
        System.out.println(this.name + ": the preparation process has started.");
        switch (this) {
            case COFFEE:
                prepareCoffee();
                break;
            case TEA:
                prepareTea();
                break;
            case LEMONADE:
                prepareLemonade();
                break;
            case MOJITO:
                prepareMojito();
                break;
            case MINERAL_WATER:
                prepareMineralWater();
                break;
            case COCA_COLA:
                prepareCocaCola();
                break;
        }
        System.out.println(this.name + ": in progress.");
        System.out.println(this.name + ": finished.");
    }
}
