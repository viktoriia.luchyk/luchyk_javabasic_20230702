package com.gmail.vluchyk;

public class ShuttleNumbers {
    public static void main(String[] args) {
        int totalShuttleCount = 100;
        int[] shuttleNumbersArray = generateShuttleNumbersArray(totalShuttleCount);
        printArray(shuttleNumbersArray);
    }

    public static int[] generateShuttleNumbersArray(int totalShuttleCount) {
        int[] unluckyNumbersArray = {4, 9};
        int runningShuttleCount = 0;
        int number = 1;
        String strNumber;
        int[] shuttleNumbersArray = new int[totalShuttleCount];
        boolean isNumberLucky;
        int lengthUnluckyNumbersArray = unluckyNumbersArray.length;
        if (lengthUnluckyNumbersArray == 0) {
            for (int i = 0; i < totalShuttleCount; i++) {
                shuttleNumbersArray[i] = i + 1;
            }
        } else {
            while (runningShuttleCount < totalShuttleCount) {
                strNumber = Integer.toString(number);
                isNumberLucky = true;
                for (int i = 0; i < lengthUnluckyNumbersArray; i++) {
                    if (strNumber.contains(Integer.toString(unluckyNumbersArray[i]))) {
                        isNumberLucky = false;
                        break;
                    }
                }
                if (isNumberLucky) {
                    shuttleNumbersArray[runningShuttleCount] = number;
                    runningShuttleCount++;
                }
                number++;
            }
        }
        return shuttleNumbersArray;
    }

    public static void printArray(int[] array) {
        int lengthArray = array.length;
        if (lengthArray > 0) {
            System.out.println("Shuttle Numbers:");
            for (int value : array) {
                System.out.printf("%d; ", value);
            }
            System.out.printf("\nTotal Count: %d", lengthArray);
        } else {
            System.out.println("None");
        }
    }
}
