package com.gmail.vluchyk.array;

import java.util.concurrent.ThreadLocalRandom;

public class MyIntArray {
    public static final int MIN_RANDOM_NUMBER = 0;
    public static final int MAX_RANDOM_NUMBER = 9;
    private int[] array;
    private int size;
    private int totalSum;
    private int minElement;

    public MyIntArray(int size) {
        this.array = new int[size];
        this.size = size;
        fillRandom(size);
        calculateTotalSum();
        findMinElement();
    }

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        if (this.array.length == array.length) {
            for (int i = 0; i < this.array.length; i++) {
                this.array[i] = array[i];
            }
        }
        calculateTotalSum();
        findMinElement();
    }

    public int getSize() {
        return size;
    }

    public int getTotalSum() {
        return totalSum;
    }

    public int getMinElement() {
        return minElement;
    }

    private void calculateTotalSum() {
        this.totalSum = 0;
        for (int i = 0; i < this.array.length; i++) {
            this.totalSum += this.array[i];
        }
    }

    private void findMinElement() {
        if (this.array.length > 0) {
            this.minElement = this.array[0];
            for (int i = 1; i < this.array.length; i++) {
                if (this.array[i] < this.minElement) {
                    this.minElement = this.array[i];
                }
            }
        }
    }

    private void fillRandom(int size) {
        for (int i = 0; i < size; i++) {
            this.array[i] = ThreadLocalRandom.current().nextInt(MIN_RANDOM_NUMBER, MAX_RANDOM_NUMBER + 1);
        }
    }

    public void divide(int divider) {
        if (divider == 0) { // якщо є можливість, то краще вказувати пряму умову. умова з запереченням сприймається гірше
            System.out.println("\nThe array is unchanged because it cannot be divided by 0.");
            return;
        }
        // позбулись "зайвого" вкладення коду
        for (int i = 0; i < this.array.length; i++) {
            this.array[i] /= divider;
        }
    }

    public void print() {
        System.out.print("[");
        for (int i = 0; i < this.array.length; i++) {
            System.out.print(this.array[i]);
            if (i < this.array.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");
    }

    public Integer[] convertToInteger() {
        Integer[] convertedArray = new Integer[this.array.length];
        for (int i = 0; i < this.array.length; i++) {
            convertedArray[i] = this.array[i];
        }
        return convertedArray;
    }

    public void quickSort(int[] array, int begin, int end) {
        if (begin < end) {
            int partitionIndex = partition(array, begin, end, false);

            quickSort(array, begin, partitionIndex - 1);
            quickSort(array, partitionIndex + 1, end);
        }
    }

    public void quickSortReverse(int[] array, int begin, int end) {
        if (begin < end) {
            int partitionIndex = partition(array, begin, end, true);

            quickSortReverse(array, begin, partitionIndex - 1);
            quickSortReverse(array, partitionIndex + 1, end);
        }
    }

    private int partition(int[] array, int begin, int end, boolean isReverse) {
        int pivot = array[end];
        int i = (begin - 1);

        for (int j = begin; j < end; j++) {
            if ((!isReverse && array[j] <= pivot) || (isReverse && array[j] >= pivot)) {
                i++;
                swap(array, i, j);
            }
        }
        swap(array, i + 1, end);

        return i + 1;
    }

    private void swap(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}
