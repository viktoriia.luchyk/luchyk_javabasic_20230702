package com.gmail.vluchyk.runner;

import com.gmail.vluchyk.matrix.MyIntMatrix;

import java.util.Scanner;

public class StepArrays {
    static int height;
    static int width;
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        dataEntry();
        MyIntMatrix matrix = new MyIntMatrix(height, width);
        matrix.printInfo("Matrix");
        matrix.print();

        MyIntMatrix sortedMatrix = new MyIntMatrix(height, width);
        sortedMatrix.setMatrix(matrix.getMatrix());
        sortedMatrix.sortByRows();
        sortedMatrix.printInfo("Sorted Matrix");
        sortedMatrix.print();

        System.out.printf("Grand Total: %d\n", matrix.getGrandTotal());

        Integer[] minElements = findMinElements(matrix.convertToInteger());
        int absoluteMin = findMinElement(minElements);
        printMinElements(matrix);

        sortedMatrix.divide(absoluteMin);
        if (absoluteMin != 0) {
            sortedMatrix.printInfo("\nDivided Matrix");
            sortedMatrix.print();
        }
        scanner.close();
    }

    private static int inputParam(String text) {
        System.out.print(text);
        return scanner.nextInt();
    }

    private static void dataEntry() {
        do {
            System.out.println("Please specify the size of the matrix (N x M):");
            height = inputParam("N (the number of rows) = ");
            width = inputParam("M (the maximum number of elements in a row) = ");
            if (height == 0 || width == 0) {
                System.out.println("The value of N or M cannot be 0");
            }
        } while (height == 0 || width == 0);
    }

    private static Integer findMinElement(Integer[] array) {
        Integer minElement = null;
        if (array.length > 0) {
            for (int i = 0; i < array.length; i++) {
                if (array[i] != null && (minElement == null || array[i] < minElement)) {
                    minElement = array[i];
                }
            }
        }
        return minElement;
    }

    private static Integer[] findMinElements(Integer[][] matrix) {
        Integer[] minArray = new Integer[matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i].length > 0) {
                minArray[i] = findMinElement(matrix[i]);
            }
        }
        return minArray;
    }

    private static void printMinElements(MyIntMatrix matrix) {
        Integer[] minElements = findMinElements(matrix.convertToInteger());
        System.out.print("The min values of the each row:");
        System.out.print("[");
        for (int i = 0; i < minElements.length; i++) {
            if (minElements[i] != null) {
                System.out.print(minElements[i]);
            } else {
                System.out.print("");
            }
            if (i < minElements.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.print("]\n");

        Integer absoluteMin = findMinElement(minElements);
        System.out.printf("The min value of the matrix: %d", absoluteMin);
    }
}
