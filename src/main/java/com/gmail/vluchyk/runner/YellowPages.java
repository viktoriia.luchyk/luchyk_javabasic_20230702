package com.gmail.vluchyk.runner;

import com.gmail.vluchyk.Person;

public class YellowPages {
    public static void main(String[] args) {
        Person[] persons = generatePersons();
        if (persons.length > 0) {
            printInfo(persons);
        } else {
            System.out.println("Інформація відсутня.");
        }

    }

    private static Person[] generatePersons() {
        int size = 3;
        Person[] persons = new Person[size];
            persons[0] = new Person("Will", "Smith", "New York", "2936729462846");
            persons[1] = new Person("Jackie", "Chan", "Shanghai", "12312412412");
            persons[2] = new Person("Sherlock", "Holmes", "London", "37742123513");
        return persons;
    }

    private static void printInfo(Person[] persons) {
        for (Person person : persons) {
            System.out.println(person.personInfo());
        }
    }
}