package com.gmail.vluchyk.runner;

import com.gmail.vluchyk.CelsiusFahrenheit;
import com.gmail.vluchyk.CelsiusKelvin;

import java.util.Scanner;

public class ConverterMain {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String answer;
        do {
            String conversionType = specifyConversionType();
            convert(conversionType);
            System.out.println();
            System.out.println("Again? Y/N:");
            scanner.nextLine();
            answer = scanner.nextLine();
        } while ("Y".equalsIgnoreCase(answer));

        scanner.close();
    }

    public static String specifyConversionType() {
        String conversionType;
        do {
            System.out.println("Specify the type of conversion.");
            System.out.println("Type CF for Celsius-Fahrenheit, FC - Fahrenheit-Celsius, CK - Celsius-Kelvin, KC - Kelvin-Celsius: ");
            conversionType = scanner.nextLine();
        } while (incorrectInput(conversionType));
        System.out.println();
        return conversionType;
    }

    private static boolean incorrectInput(String conversionType) {
        if (!"CF".equalsIgnoreCase(conversionType) && !"FC".equalsIgnoreCase(conversionType) && !"CK".equalsIgnoreCase(conversionType) && !"KC".equalsIgnoreCase(conversionType)) {
            System.out.println("Incorrect input. Try again.");
            return true;
        }
        return false;
    }

    public static void convert(String conversionType) {
        if ("CF".equalsIgnoreCase(conversionType)) {
            convertCF();
        } else if ("FC".equalsIgnoreCase(conversionType)) {
            convertFC();
        } else if ("CK".equalsIgnoreCase(conversionType)) {
            convertCK();
        } else if ("KC".equalsIgnoreCase(conversionType)) {
            convertKC();
        }
    }

    public static void convertCF() {
        System.out.println("Specify the value in Celsius:");
        double value = scanner.nextDouble();
        CelsiusFahrenheit celsius = new CelsiusFahrenheit();
        celsius.setCelsius(value);
        System.out.printf("The value in Fahrenheit: %.2f", celsius.getFahrenheit());
    }

    private static void convertFC() {
        System.out.println("Specify the value in Fahrenheit: ");
        double value = scanner.nextDouble();
        CelsiusFahrenheit fahrenheit = new CelsiusFahrenheit();
        fahrenheit.setFahrenheit(value);
        System.out.printf("The value in Celsius: %.2f", fahrenheit.getCelsius());
    }

    private static void convertCK() {
        System.out.println("Specify the value in Celsius: ");
        double value = scanner.nextDouble();
        CelsiusKelvin celsius = new CelsiusKelvin();
        celsius.setCelsius(value);
        System.out.printf("The value in Kelvin: %.2f", celsius.getKelvin());
    }

    private static void convertKC() {
        System.out.println("Specify the value in Kelvin: ");
        double value = scanner.nextDouble();
        CelsiusKelvin kelvin = new CelsiusKelvin();
        kelvin.setKelvin(value);
        System.out.printf("The value in Celsius: %.2f", kelvin.getCelsius());
    }
}