package com.gmail.vluchyk.runner;

import com.gmail.vluchyk.User;
import com.gmail.vluchyk.exceptions.WrongLoginException;
import com.gmail.vluchyk.exceptions.WrongPasswordException;

import java.util.Scanner;

public class UserMain {
    private static Scanner scanner = new Scanner(System.in);
    private static final int ATTEMPTS_NUMBER = 3;

    public static void main(String[] args) {
        createUser();
        System.out.println("Thank you for using our service.");
        scanner.close();
    }

    private static void createUser() {
        int attemptsCount = 1;
        String message = "";
        User user = null;
        while (attemptsCount <= ATTEMPTS_NUMBER) {
            try {
                user = inputData();
            } catch (WrongLoginException | WrongPasswordException exception) {
                message = exception.getMessage();
                System.out.println(message);
            } finally {
                if ("".equalsIgnoreCase(message)) {
                    System.out.println("Account is successfully created.");
                    return;
                } else {
                    System.out.println("Attempts left: " + (ATTEMPTS_NUMBER - attemptsCount));
                    System.out.println();
                }
            }
            if (attemptsCount == ATTEMPTS_NUMBER && user == null) {
                System.out.println("Sorry, your account is not created.");
            }
            attemptsCount++;
        }
    }

    private static User inputData() throws WrongLoginException, WrongPasswordException {
        System.out.println("Please register your account.");
        System.out.print("Enter login: ");
        String login = scanner.nextLine();

        System.out.print("Enter password: ");
        String password = scanner.nextLine();

        System.out.print("Confirm password: ");
        String confirmPassword = scanner.nextLine();

        return new User(login, password, confirmPassword);
    }
}
