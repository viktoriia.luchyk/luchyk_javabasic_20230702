package com.gmail.vluchyk.runner;

import com.gmail.vluchyk.Burger;

public class BurgerMain {
    public static void main(String[] args) {
        Burger origin = new Burger(true);
        Burger diet = new Burger();
        Burger doubleMeat = new Burger(2);
    }
}