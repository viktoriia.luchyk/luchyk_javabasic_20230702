package com.gmail.vluchyk.runner;

import com.gmail.vluchyk.music.ClassicMusic;
import com.gmail.vluchyk.music.MusicStyles;
import com.gmail.vluchyk.music.PopMusic;
import com.gmail.vluchyk.music.RockMusic;

public class MusicMain {
    private static MusicStyles[] bands = {
            new PopMusic("Pop Band"),
            new RockMusic("Rock Band"),
            new ClassicMusic("Classic Band")
    };

    public static void main(String[] args) {
        for (MusicStyles band : bands) {
            band.playMusic();
        }
    }
}