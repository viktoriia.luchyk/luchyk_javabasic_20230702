package com.gmail.vluchyk.runner;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class ArraysMain {
    public static final double MIN_RANDOM_NUMBER = -100;
    public static final double MAX_RANDOM_NUMBER = 100;

    public static void main(String[] args) {
        int size = 5;
        printAverageOfArray(size);
        System.out.println();
    }

    public static double[] generateArray(int size) {
        if (size <= 0) {
            System.out.println("The array is not created.");
            return new double[0];
        }
        double[] array = new double[size];
        for (int i = 0; i < array.length; i++) {
            array[i] = Math.round(ThreadLocalRandom.current().nextDouble(MIN_RANDOM_NUMBER, MAX_RANDOM_NUMBER + 1) * 100.0) / 100.0;
        }
        return array;
    }

    public static double average(double[] array) {
        if (array == null || array.length == 0) {
            System.out.println("The array doesn't exist.");
            return 0;
        }
        double total = 0;
        for (int i = 0; i < array.length; i++) {
            total += array[i];
        }
        return total / array.length;
    }

    public static void print(double[] array) {
        System.out.print("Array: ");
        System.out.println(Arrays.toString(array));
    }

    public static void printAverageOfArray(int size) {
        double[] array = generateArray(size);
        print(array);
        double averageValue = average(array);
        System.out.printf("Average = %.2f", averageValue);
    }

    public static boolean isMatrixSquare(double[][] matrix) {
        if (matrix == null || matrix.length == 0) {
            System.out.println("Matrix doesn't exist.");
            return false;
        }
        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i] == null || matrix[i].length != matrix.length) {
                return false;
            }
        }
        return true;
    }
}
