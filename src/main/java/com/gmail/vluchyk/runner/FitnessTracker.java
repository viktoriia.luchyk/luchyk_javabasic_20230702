package com.gmail.vluchyk.runner;

import com.gmail.vluchyk.Account;

public class FitnessTracker {
    public static void main(String[] args) {
        Account firstUser = new Account("John", "Smith", 1985, 3, 20, "john.smith@gmail.com", "+380662920101", 82, "120/80", 2000);
        firstUser.printAccountInfo();
        System.out.println();

        Account secondUser = new Account("Mery", "Hill", 1978, 7, 23, "mery.hill@gmail.com", "+380672561213", 58, "115/82", 3234);
        secondUser.printAccountInfo();
        System.out.println();

        Account thirdUser = new Account("Tom", "Taylor", 1990, 2, 28, "ttaylor@outlook.com", "+380504852625", 79, "125/85", 2500);
        thirdUser.printAccountInfo();
        System.out.println();

        System.out.println("MODIFIED DATA");

        firstUser.setWeight(80);
        firstUser.setStepsNumber(2100);
        firstUser.printAccountInfo();
        System.out.println();

        secondUser.setLastName("Black");
        secondUser.setPressure("118/78");
        secondUser.printAccountInfo();
        System.out.println();
    }
}
