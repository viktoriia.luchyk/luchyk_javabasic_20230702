package com.gmail.vluchyk.runner;

import com.gmail.vluchyk.interfaces.Smartphones;
import com.gmail.vluchyk.phones.Androids;
import com.gmail.vluchyk.phones.IPhones;

public class PhoneInfo {
    public static void main(String[] args) {
        System.out.println("__________ Androids __________");
        printAndroidInfo(null);
        printAndroidInfo("+380503121213");

        System.out.println("__________ iPhones __________");
        printIPhoneInfo(null);
        printIPhoneInfo("John Smith");

        System.out.println("__________ Smartphones __________");
        printSmartphonesInfo(null, null);
        printSmartphonesInfo("+380662920102", "Tom Taylor");
    }

    private static void printAndroidInfo(String phoneNumber) {
        Androids phone = new Androids(phoneNumber);
        phone.installOS();
        phone.updateOS();
        phone.installThirdPartyApp();
        phone.useMemoryCard();
        phone.call();
        phone.sms();
        phone.internet();
        System.out.println();
    }

    private static void printIPhoneInfo(String owner) {
        IPhones phone = new IPhones(owner);
        phone.installOS();
        phone.updateOS();
        phone.airDrop();
        phone.synchronize();
        phone.call();
        phone.sms();
        phone.internet();
        System.out.println();
    }

    private static void printSmartphonesInfo(String androidPhoneNumber, String iPhoneOwner) {
        Smartphones[] phones = {new Androids(androidPhoneNumber), new IPhones(iPhoneOwner)};
        for (Smartphones phone : phones) {
            phone.call();
            phone.sms();
            phone.internet();
            System.out.println();
        }
    }
}
