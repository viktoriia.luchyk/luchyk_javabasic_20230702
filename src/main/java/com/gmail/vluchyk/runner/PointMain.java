package com.gmail.vluchyk.runner;

import com.gmail.vluchyk.Point;

public class PointMain {
    public static void main(String[] args) throws CloneNotSupportedException {
        Point firstPoint = new Point("A", 2, 3);
        Point secondPoint = new Point("B", 4, 5);

        System.out.println("Initial data:");
        System.out.println(firstPoint);
        System.out.println(secondPoint);

        printDistances(firstPoint, secondPoint);

        changeCoordinates(firstPoint, 4, 5);
        comparePoints(firstPoint, secondPoint);

        changeCoordinates(secondPoint, 6, 7);
        comparePoints(firstPoint, secondPoint);

        Point anotherPoint = new Point(firstPoint);
        Point clonedPoint = firstPoint.clone();

        System.out.printf("Cloned data from %s:\n", firstPoint.getName());
        printPointInfo("Another point: ", anotherPoint);
        printPointInfo("Cloned point: ", clonedPoint);
        System.out.println();

        changeCoordinates(firstPoint, 10, 15);
        printPointInfo("Another point: ", anotherPoint);
        printPointInfo("Cloned point: ", clonedPoint);
        System.out.println();

        anotherPoint.setName("C");
        clonedPoint.setName("D");
        printPointInfo("Another point: ", anotherPoint);
        printPointInfo("Cloned point: ", clonedPoint);
        System.out.println();

        comparePoints(anotherPoint, clonedPoint);
        comparePoints(clonedPoint, clonedPoint);
    }

    private static void printDistances(Point firstPoint, Point secondPoint) {
        if (Point.check(firstPoint) && Point.check(secondPoint)) {
            System.out.printf("The distance from %s to %s = %.1f.\n", firstPoint.getName(), secondPoint.getName(), firstPoint.distance(secondPoint));
            System.out.printf("The distance between %s and %s = %.1f.\n", firstPoint.getName(), secondPoint.getName(), Point.distance(firstPoint, secondPoint));
            System.out.println();
        } else {
            System.out.println("The distance can't be found. Some of the points don't exist.");
        }
    }

    private static void changeCoordinates(Point point, int x, int y) {
        if (Point.check(point)) {
            System.out.printf("Changed coordinates of %s:\n", point.getName());
            point.setX(x);
            point.setY(y);
            System.out.println(point);
        } else {
            System.out.println("It's impossible to change coordinates. The point doesn't exist.");
        }
    }

    private static void comparePoints(Point firstPoint, Point secondPoint) {
        if (Point.check(firstPoint) && Point.check(secondPoint)) {
            System.out.printf("Are the coordinates of %s and %s equal? %b\n", firstPoint.getName(), secondPoint.getName(), firstPoint.equals(secondPoint));
            System.out.printf("Are the references of %s and %s equal? %b\n", firstPoint.getName(), secondPoint.getName(), firstPoint == secondPoint);
            System.out.println();
        } else {
            System.out.println("It's impossible to compare the points. Some of the points don't exist.");
        }
    }

    private static void printPointInfo(String text, Point point) {
        if (Point.check(point)) {
            System.out.print(text);
            System.out.println(point);
        } else {
            System.out.println("The point doesn't exist.");
        }
    }
}