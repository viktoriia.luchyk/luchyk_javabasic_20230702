package com.gmail.vluchyk;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class Rugby {
    static int minRandomNumber = 18;
    static int maxRandomNumber = 40;
    static int countOfPlayers = 25;

    public static void main(String[] args) {
        int[] firstTeam = generateArray(countOfPlayers);
        int[] secondTeam = generateArray(countOfPlayers);
        System.out.println("Rugby Teams:");
        System.out.print("The 1st team players age: ");
        printArray(firstTeam);
        System.out.print("The 2nd team players age: ");
        printArray(secondTeam);
        System.out.print("The 1st team average age: ");
        System.out.println(averageInArray(firstTeam));
        System.out.print("The 2nd team average age: ");
        System.out.println(averageInArray(secondTeam));
    }

    public static int[] generateArray(int length) {
        int[] array = new int[length];
        for (int i = 0; i < array.length; i++) {
            array[i] = ThreadLocalRandom.current().nextInt(minRandomNumber, maxRandomNumber + 1);
        }
        return array;
    }

    public static void printArray(int[] array) {
        System.out.println(Arrays.toString(array));
    }

    public static int averageInArray(int[] array) {
        if (array.length == 0) {
            return 0;
        } else {
            int totalAge = 0;
            for (int value : array) {
                totalAge += value;
            }
            return totalAge / array.length;
        }
    }
    //Add this comment to check Commit and Push functionality
}
