package com.gmail.vluchyk;

public class Dynasty {
    private String name;
    private int warriorAttackRate;
    private int bowmanAttackRate;
    private int riderAttackRate;
    private int count;

    public Dynasty(String name, int warriorAttackRate, int bowmanAttackRate, int riderAttackRate, int count) {
        this.name = name;
        this.warriorAttackRate = warriorAttackRate;
        this.bowmanAttackRate = bowmanAttackRate;
        this.riderAttackRate = riderAttackRate;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWarriorAttackRate() {
        return warriorAttackRate;
    }

    public void setWarriorAttackRate(int warriorAttackRate) {
        this.warriorAttackRate = warriorAttackRate;
    }

    public int getBowmanAttackRate() {
        return bowmanAttackRate;
    }

    public void setBowmanAttackRate(int bowmanAttackRate) {
        this.bowmanAttackRate = bowmanAttackRate;
    }

    public int getRiderAttackRate() {
        return riderAttackRate;
    }

    public void setRiderAttackRate(int riderAttackRate) {
        this.riderAttackRate = riderAttackRate;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public long getDynastyAttackRate() {
        return (long) (this.getWarriorAttackRate() + this.getBowmanAttackRate() + this.getRiderAttackRate()) * this.getCount();
    }
}
