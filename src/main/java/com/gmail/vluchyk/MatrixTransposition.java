package com.gmail.vluchyk;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class MatrixTransposition {
    static Scanner scanner = new Scanner(System.in);
    static int minRandomNumber = 0;
    static int maxRandomNumber = 9;

    public static void main(String[] args) {
        int[][] matrix = createMatrix();
        fillMatrix(matrix);
        printInfo(matrix, "Matrix");
        printMatrix(matrix);

        int[][] transposedMatrix = transpose(matrix);
        printInfo(transposedMatrix, "Transposed Matrix");
        printMatrix(transposedMatrix);

        scanner.close();
    }

    public static int[][] createMatrix() {
        System.out.println("Please specify the size of the matrix (M x N):");
        int width = getInputtedValue("M = ");
        int height = getInputtedValue("N = ");
        return new int[height][width];
    }

    public static int getInputtedValue(String paramsName) {
        int value;
        do {
            System.out.print(paramsName);
            value = scanner.nextInt();
            if (value < 1) {
                System.out.println("The entered value cannot be less than 1.");
            }
        } while (value < 1);
        return value;
    }

    public static void fillMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = ThreadLocalRandom.current().nextInt(minRandomNumber, maxRandomNumber + 1);
            }
        }
    }

    public static void printInfo(int[][] matrix, String name) {
        System.out.printf(name + " (%d x %d):\n", matrix[0].length, matrix.length);
    }

    public static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            System.out.print("[");
            for (int j = 0; j < matrix[i].length; j++) {
/*
                if (j < matrix[i].length - 1) {
                    System.out.print(matrix[i][j] + ", ");
                } else if (j == matrix[i].length - 1) {
                    System.out.print(matrix[i][j]);
                }
 */
                System.out.print(matrix[i][j]);
                if (j < matrix[i].length - 1) {
                    System.out.print(", ");
                }
            }
            System.out.print("]\n");
        }
    }

    public static int[][] transpose(int[][] matrix) {
        int[][] transposedMatrix = new int[matrix[0].length][matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                transposedMatrix[j][i] = matrix[i][j];
            }
        }
        return transposedMatrix;
    }
}
