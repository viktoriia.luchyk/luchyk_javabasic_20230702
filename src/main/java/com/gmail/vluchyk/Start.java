package com.gmail.vluchyk;

import java.util.Scanner;

public class Start {
    public static void main(String[] args) {
        Student student = new Student(18, "John");
        String studentName = student.getName();
        int studentAge = student.getAge();

        System.out.println("Enter the student name:");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();

        System.out.println("Enter the student age:");
        int age = Integer.parseInt(scanner.nextLine());

        while (!(studentName.toLowerCase().equals(name.toLowerCase()) && studentAge == age)) {
            if (!studentName.toLowerCase().equals(name.toLowerCase()) && studentAge != age) {
                System.out.println("There is no such student. Please try again.");
                System.out.println("Enter the student name: ");
                name = scanner.nextLine();
                System.out.println("Enter the student age: ");
                age = Integer.parseInt(scanner.nextLine());
            } else if (!studentName.toLowerCase().equals(name.toLowerCase())) {
                System.out.println("The student name is incorrect. Enter the name again:");
                name = scanner.nextLine();
            } else if (studentAge != age) {
                System.out.println("The student age is incorrect. Enter the age again:");
                age = Integer.parseInt(scanner.nextLine());
            }

        }
        System.out.println("Correct! The student " + studentName + " (age:" + studentAge + ") exists in the system.");

        scanner.close();
    }
}
