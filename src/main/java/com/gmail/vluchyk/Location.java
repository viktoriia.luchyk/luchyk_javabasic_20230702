package com.gmail.vluchyk;

public class Location {
    public static void main(String[] args) {
        double latitude = 50.450266546595934;
        double longitude = 30.51306453970693;

        System.out.println("Location of the Golden Gates: (" + latitude + ", " + longitude + ")");
    }
}
