package com.gmail.vluchyk;

import java.util.concurrent.ThreadLocalRandom;

public class Lottery {
    static int size = 7;
    static int minRandomNumber = 0;
    static int maxRandomNumber = 9;

    public static void main(String[] args) {
        int[] guessedNumbersArray = generateArray(size);
        System.out.print("Guessed numbers: ");
        printArray(guessedNumbersArray);

        int[] proposedNumbersArray = generateArray(size);
        System.out.print("\nProposed numbers: ");
        printArray(proposedNumbersArray);

        quickSort(guessedNumbersArray, 0, guessedNumbersArray.length - 1);
        System.out.print("\nGuessed numbers (sorted): ");
        printArray(guessedNumbersArray);

        quickSort(proposedNumbersArray, 0, proposedNumbersArray.length - 1);
        System.out.print("\nProposed numbers (sorted): ");
        printArray(proposedNumbersArray);

        System.out.printf("\nNumber of matches: %d", getMatchesNumberInArrays(guessedNumbersArray, proposedNumbersArray));
    }

    public static int[] generateArray(int size) {
        int[] array = new int[size];
        for (int i = 0; i < array.length; i++) {
            array[i] = ThreadLocalRandom.current().nextInt(minRandomNumber, maxRandomNumber + 1);
        }
        return array;
    }

    public static void printArray(int[] array) {
        System.out.print("[");
        for (int i = 0; i < array.length; i++) {
            if (i < array.length - 1) {
                System.out.print(array[i] + ", ");
            } else if (i == array.length - 1) {
                System.out.print(array[i]);
            }
        }
        System.out.print("]");
    }

    public static int getMatchesNumberInArrays(int[] firstArray, int[] secondArray) {
        int matchesCount = 0;
        int length = Math.min(firstArray.length, secondArray.length);
        for (int i = 0; i < length; i++) {
            matchesCount += (firstArray[i] == secondArray[i]) ? 1 : 0;
        }
        return matchesCount;
    }

    public static void quickSort(int[] array, int first, int last) {
        int left = first;
        int right = last;
        int pivot = array[(left + right) / 2];
        while (left <= right) {
            while (array[left] < pivot) {
                left++;
            }
            while (array[right] > pivot) {
                right--;
            }
            if (left <= right) {
                int temp = array[left];
                array[left] = array[right];
                array[right] = temp;
                left++;
                right--;
            }
        }
        if (first < right) {
            quickSort(array, first, right);
        }
        if (left < last) {
            quickSort(array, left, last);
        }
    }
}
