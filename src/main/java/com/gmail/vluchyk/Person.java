package com.gmail.vluchyk;

public class Person {
    private String firstName;
    private String lastName;
    private String city;
    private String phoneNumber;

    public Person(String firstName, String lastName, String city, String phoneNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
        this.phoneNumber = phoneNumber;
    }

    private boolean isNotEmpty(String str) {
        return str != null && !str.equals("");
    }

    private String displayText(String str) {
        return isNotEmpty(str) ? str : "____________";
    }

    public String getFullName() {
        String fullName = "";
        if (isNotEmpty(this.firstName)) {
            fullName = this.firstName;
        }
        if (isNotEmpty(this.lastName)) {
            fullName += (isNotEmpty(fullName)) ? " " + this.lastName : this.lastName;
        }
        return fullName;
    }

    public String personInfo() {
        String infoText = "Зателефонувати громадянинові ";
        String fullName = getFullName();
        infoText += displayText(fullName);
        infoText += " з міста ";
        infoText += displayText(this.city);
        infoText += " можна за номером ";
        infoText += displayText(this.phoneNumber);
        return infoText;
    }
}