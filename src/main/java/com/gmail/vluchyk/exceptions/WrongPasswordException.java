package com.gmail.vluchyk.exceptions;

public class WrongPasswordException extends IllegalArgumentException {
    public WrongPasswordException(String message) {
        super(message);
    }
}
