package com.gmail.vluchyk.exceptions;

public class WrongLoginException extends IllegalArgumentException {
    public WrongLoginException(String message) {
        super(message);
    }
}
