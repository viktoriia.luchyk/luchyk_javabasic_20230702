package com.gmail.vluchyk.list;

public class MyListException extends RuntimeException {
    public MyListException(String message) {
        super(message);
    }
}
