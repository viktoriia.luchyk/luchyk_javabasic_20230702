package com.gmail.vluchyk;

public class ChineseDynasties {
    public static void main(String[] args) {
        Dynasty dynastyLi = new Dynasty("Li", 13, 24, 46, 860);
        int dynastyLiCount = dynastyLi.getCount();
        if (dynastyLiCount > 0) {
            int dynastyMingCount = (int) (1.5 * dynastyLiCount);
            Dynasty dynastyMing = new Dynasty("Ming", 9, 35, 12, dynastyMingCount);
            long dynastyLiAttackRate = dynastyLi.getDynastyAttackRate();
            long dynastyMingAttackRate = dynastyMing.getDynastyAttackRate();
            String dynastyLiInfo = String.format("The total attack rate of the %s dynasty is %,d.", dynastyLi.getName(), dynastyLiAttackRate);
            String dynastyMingInfo = String.format("The total attack rate of the %s dynasty is %,d.", dynastyMing.getName(), dynastyMingAttackRate);
            System.out.println(dynastyLiInfo);
            System.out.println(dynastyMingInfo);
        } else {
            System.out.println("The count of Li dynasty can't be 0.");
        }
    }
}
