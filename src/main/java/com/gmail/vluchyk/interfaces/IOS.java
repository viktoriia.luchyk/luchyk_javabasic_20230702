package com.gmail.vluchyk.interfaces;

public interface IOS {
    void installOS();

    void updateOS();

    void airDrop();

    void synchronize();
}
