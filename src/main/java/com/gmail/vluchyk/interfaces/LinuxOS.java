package com.gmail.vluchyk.interfaces;

public interface LinuxOS {
    void installOS();

    void updateOS();

    void installThirdPartyApp();

    void useMemoryCard();
}
