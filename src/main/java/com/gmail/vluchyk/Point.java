package com.gmail.vluchyk;

import java.util.Objects;

public class Point implements Cloneable {
    private String name;
    private int x;
    private int y;

    public Point(String name, int x, int y) {
        this.name = (name == null) ? "" : name;
        this.x = x;
        this.y = y;
    }

    public Point(Point point) {
        this.name = (point.name == null) ? "" : point.name;
        this.x = point.x;
        this.y = point.y;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = (name == null) ? "" : name;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public double distance(Point point) {
        if (point == null) {
            System.out.println("The distance can't be found. The second point does not exist.");
            return 0;
        }
        return Math.sqrt(Math.pow(point.x - this.x, 2) + Math.pow(point.y - this.y, 2));
    }

    public static double distance(Point firstPoint, Point secondPoint) {
        if (firstPoint == null) {
            System.out.println("The distance can't be found. The first point does not exist.");
            return 0;
        }
        if (secondPoint == null) {
            System.out.println("The distance can't be found. The second point does not exist.");
            return 0;
        }
        return firstPoint.distance(secondPoint);
    }

    public static boolean check(Point point) {
        return point != null;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Point point = (Point) obj;

        //return this.x == point.x && this.y == point.y; // this row can be used too since x and y are variables of type int.
        return Objects.equals(x, point.x) && Objects.equals(y, point.y);
    }

    @Override
    public int hashCode() {
        //Java 8+. This option involves the use of a helper method for generating a hash code, which is available in the java.util.Objects class.
        return Objects.hash(x, y);

        /*
        //The second option is using an existing algorithm to implement own override.
        int total = 31;

        total = total * 31 + x;
        total = total * 31 + y;

        return total;
         */
    }

    @Override
    public String toString() {
        return this.name + "(" + this.x + ", " + this.y + ");";
    }

    @Override
    public Point clone() throws CloneNotSupportedException {
        return (Point) super.clone();
    }
}