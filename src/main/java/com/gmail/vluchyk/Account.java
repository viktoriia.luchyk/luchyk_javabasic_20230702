package com.gmail.vluchyk;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Account {
    private final String firstName;
    private String lastName;
    private final int yearOfBirth;
    private final int monthOfBirth;
    private final int dayOfBirth;
    private final String email;
    private final String phone;
    private int weight;
    private String pressure;
    private int stepsNumber;
    private int age;

    public Account(String firstName, String lastName, int yearOfBirth, int monthOfBirth, int dayOfBirth, String email, String phone, int weight, String pressure, int stepsNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.yearOfBirth = yearOfBirth;
        this.monthOfBirth = monthOfBirth;
        this.dayOfBirth = dayOfBirth;
        this.email = email;
        this.phone = phone;
        this.weight = weight;
        this.pressure = pressure;
        this.stepsNumber = stepsNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public int getMonthOfBirth() {
        return monthOfBirth;
    }

    public int getDayOfBirth() {
        return dayOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public int getStepsNumber() {
        return stepsNumber;
    }

    public void setStepsNumber(int stepsNumber) {
        this.stepsNumber = stepsNumber;
    }

    public int getAge() {
        return age;
    }

    public void printAccountInfo() {
        System.out.println("Account Info:");
        System.out.println("\tFull Name: " + fullName(this.firstName, this.lastName));
        System.out.println("\tBirth Date: " + birthDate(this.yearOfBirth, this.monthOfBirth, this.dayOfBirth));
        calculateAge(this.yearOfBirth);
        System.out.println("\tAge: " + this.age);
        System.out.println("\tEmail: " + checkEmail(this.email));
        System.out.println("\tPhone: " + this.phone);
        System.out.println("\tWeight: " + this.weight);
        System.out.println("\tPressure: " + this.pressure);
        System.out.println("\tNumber of steps: " + this.stepsNumber);
    }

    private String fullName(String firstName, String lastName) {
        if (notEmpty(firstName) && notEmpty(lastName)) {
            return firstName + " " + lastName;
        }
        if (notEmpty(firstName)) {
            return firstName;
        }
        if (notEmpty(lastName)) {
            return lastName;
        } else {
            return "";
        }
    }

    private String birthDate(int yearOfBirth, int monthOfBirth, int dayOfBirth) {
        String warningMsg = dateWarningMessage(yearOfBirth, monthOfBirth, dayOfBirth);
        if (warningMsg == null) {
            LocalDate date = LocalDate.of(yearOfBirth, monthOfBirth, dayOfBirth);
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy", Locale.US);
            return date.format(dtf);
        }
        return warningMsg;
    }

    private String dateWarningMessage(int yearOfBirth, int monthOfBirth, int dayOfBirth) {
        if (yearOfBirth <= 0 || yearOfBirth > currentYear()) {
            return "The year of the birth for the user is incorrect. Please enter the correct value.";
        }
        if (monthOfBirth <= 0 || monthOfBirth > 12) {
            return "The month of the birth for the user is incorrect. Please enter the correct value.";
        }
        if (dayOfBirth <= 0
                || ("1,3,5,7,8,10,12".contains(Integer.toString(monthOfBirth)) && dayOfBirth > 31)
                || ("4,6,9,11".contains(Integer.toString(monthOfBirth)) && dayOfBirth > 30)
                || ("2".contains(Integer.toString(monthOfBirth)) && (leapYear(yearOfBirth) && dayOfBirth > 29 || !leapYear(yearOfBirth) && dayOfBirth > 28))
        ) {
            return "The day of the birth for the user is incorrect. Please enter the correct value.";
        }
        return null;
    }

    private boolean leapYear(int yearOfBirth) {
        if (yearOfBirth < 0) {
            return false;
        }
        if (yearOfBirth % 4 == 0) {
            if (yearOfBirth % 100 == 0 && yearOfBirth % 400 != 0) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    private String checkEmail(String email) {
        String warningMsg = emailWarningMessage(this.email);
        if (warningMsg == null) {
            return this.email;
        }
        return warningMsg;
    }

    private String emailWarningMessage(String email) {
        if (notEmpty(email) && email.contains("@")) {
            return null;
        }
        if (notEmpty(email)) {
            return "The email is incorrect. Please enter the correct value.";
        }
        return "The email is not specified";
    }

    private void calculateAge(int yearOfBirth) {
        int currentYear = currentYear();
        this.age = currentYear - yearOfBirth;
    }

    private int currentYear() {
        LocalDate currentDate = LocalDate.now();
        return currentDate.getYear();
    }

    private boolean notEmpty(String str) {
        return str != null && !str.equals("");
    }
}
