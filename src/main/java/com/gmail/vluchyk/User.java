package com.gmail.vluchyk;

import com.gmail.vluchyk.exceptions.WrongLoginException;
import com.gmail.vluchyk.exceptions.WrongPasswordException;

public class User {
    private static final String LOGIN_REGEX = "^[a-zA-Z]{1,20}$";
    private static final String PASSWORD_REGEX = "^(?=.*[0-9])(?=.*[a-zA-Z])[a-zA-z\\d]{6,25}$";
    private String login;
    private String password;

    public User(String login, String password, String confirmedPassword) {
        checkLogin(login);
        this.login = login;

        checkPassword(password);
        checkConfirmedPassword(password, confirmedPassword);
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        checkLogin(login);
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password, String confirmedPassword) {
        checkPassword(password);
        checkConfirmedPassword(password, confirmedPassword);
        this.password = password;
    }

    private void checkLogin(String login) {
        if (isEmpty(login)) {
            throw new WrongLoginException("The login is not specified.");
        } else if (isLoginIncorrect(login)) {
            throw new WrongLoginException("Wrong login: Login must be no more than 20 characters long and contain only Latin letters.");
        }
    }

    private boolean isLoginIncorrect(String login) {
        return !login.matches(LOGIN_REGEX);
    }

    private void checkPassword(String password) {
        if (isEmpty(password)) {
            throw new WrongPasswordException("The password is not specified.");
        } else if (isPasswordIncorrect(password)) {
            throw new WrongPasswordException("Wrong password: The password must contain Latin letters and numbers. Must be at least 1 letter and 1 number. The length of the password is from 6 to 25 characters.");
        }
    }

    private boolean isPasswordIncorrect(String password) {
        return !password.matches(PASSWORD_REGEX);
    }

    private static void checkConfirmedPassword(String password, String confirmedPassword) {
        if (!password.equals(confirmedPassword)) {
            throw new WrongPasswordException("The confirmed password doesn't match the password.");
        }
    }

    private static boolean isEmpty(String str) {
        return (str == null || "".equals(str));
    }
}
