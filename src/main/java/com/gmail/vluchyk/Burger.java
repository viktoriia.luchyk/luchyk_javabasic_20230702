package com.gmail.vluchyk;

public class Burger {
    private String name;
    private boolean roll;
    private boolean meat;
    private int meatNumber;
    private boolean cheese;
    private boolean green;
    private boolean mayonnaise;

    public Burger() {
        this.name = "Diet Burger";
        this.roll = true;
        this.meat = true;
        this.meatNumber = 1;
        this.cheese = true;
        this.green = true;
        this.mayonnaise = false;

        printComponents();
    }

    public Burger(boolean mayonnaise) {
        this.name = (mayonnaise) ? "Origin Burger" : "Diet Burger";
        this.roll = true;
        this.meat = true;
        this.meatNumber = 1;
        this.cheese = true;
        this.green = true;
        this.mayonnaise = mayonnaise;

        printComponents();
    }

    public Burger(int meatNumber) {
        this.name = (meatNumber > 1) ? "Burger with double meat" : "Origin Burger";
        this.roll = true;
        this.meat = true;
        this.meatNumber = (meatNumber >= 1) ? meatNumber : 1;
        this.cheese = true;
        this.green = true;
        this.mayonnaise = true;

        printComponents();
    }

    private void printComponents() {
        System.out.printf("The composition of the %s: ", this.name);
        System.out.print("roll");
        if (this.meatNumber > 1) {
            System.out.print(", " + this.meatNumber + " meats");
        } else {
            System.out.print(", meat");
        }
        System.out.print(", cheese");
        System.out.print(", green");
        if (this.mayonnaise) {
            System.out.print(", mayonnaise");
        }
        System.out.print(".\n");
    }
}
