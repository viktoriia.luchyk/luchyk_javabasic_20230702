package com.gmail.vluchyk;

public class CelsiusKelvin extends AbstractConverter {
    private double celsius;
    private double kelvin;

    public CelsiusKelvin() {
    }

    public double getCelsius() {
        return celsius;
    }

    public void setCelsius(double celsius) {
        this.celsius = celsius;
        convertFromCelsius();
    }

    public double getKelvin() {
        return kelvin;
    }

    public void setKelvin(double kelvin) {
        this.kelvin = kelvin;
        convertToCelsius();
    }

    @Override
    protected void convertFromCelsius() {
        this.kelvin = this.celsius + 273.15;
    }

    @Override
    protected void convertToCelsius() {
        this.celsius = this.kelvin - 273.15;
    }
}