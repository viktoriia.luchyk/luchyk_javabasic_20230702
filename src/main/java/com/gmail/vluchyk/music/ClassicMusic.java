package com.gmail.vluchyk.music;

public class ClassicMusic extends MusicStyles {
    public ClassicMusic(String bandName) {
        super(bandName);
    }

    @Override
    public void playMusic() {
        System.out.println(this.bandName + ": Classic music is playing.");
    }
}