package com.gmail.vluchyk.music;

public class PopMusic extends MusicStyles {
    public PopMusic(String bandName) {
        super(bandName);
    }

    @Override
    public void playMusic() {
        System.out.println(this.bandName + ": Pop music is playing.");
    }
}