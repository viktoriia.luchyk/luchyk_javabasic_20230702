package com.gmail.vluchyk.music;

public abstract class MusicStyles {
    protected String bandName;

    public MusicStyles(String bandName) {
        this.bandName = bandName;
    }

    public abstract void playMusic();
}