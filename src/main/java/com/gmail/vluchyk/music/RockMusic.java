package com.gmail.vluchyk.music;

public class RockMusic extends MusicStyles {
    public RockMusic(String bandName) {
        super(bandName);
    }

    @Override
    public void playMusic() {
        System.out.println(this.bandName + ": Rock music is playing.");
    }
}