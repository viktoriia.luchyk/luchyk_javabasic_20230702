package com.gmail.vluchyk;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class GuessNumberGame {
    static Scanner scanner = new Scanner(System.in);
    static int minRandomNumber = 1;
    static int maxRandomNumber = 10;
    static int easyLevelAttemptsNumber = 7;
    static int mediumLevelAttemptsNumber = 5;
    static int heavyLevelAttemptsNumber = 3;

    public static void main(String[] args) {
        System.out.println("Welcome to the 'Guess the Number' game.");
        printRules();
        String answerPlayGame = "YES";
        do {
            play();
            System.out.println("Play again? Yes/No:");
            scanner.nextLine();
            answerPlayGame = scanner.nextLine();
        } while ("Yes".equalsIgnoreCase(answerPlayGame));
    }

    public static void printRules() {
        System.out.printf("Rules: it is necessary to guess the number from %d to %d inclusive. The number of attempts depends on the level of the game.\n", minRandomNumber, maxRandomNumber);
        System.out.printf("Easy: %d attempts;\n", easyLevelAttemptsNumber);
        System.out.printf("Medium: %d attempts;\n", mediumLevelAttemptsNumber);
        System.out.printf("Heavy: %d attempts.\n", heavyLevelAttemptsNumber);
    }

    public static int getLevelAttemptsNumber() {
        String level;
        boolean isEnterCorrectly;
        do {
            System.out.println("Enter the level of the game (Easy, Medium, Heavy):");
            level = scanner.nextLine();
            isEnterCorrectly = level.equalsIgnoreCase("Easy") || level.equalsIgnoreCase("Medium") || level.equalsIgnoreCase("Heavy");
            if (!isEnterCorrectly) {
                System.out.println("The level is incorrect! Please enter again:");
            }
        } while (!isEnterCorrectly);
        if (level.equalsIgnoreCase("Easy")) {
            return easyLevelAttemptsNumber;
        } else if (level.equalsIgnoreCase("Medium")) {
            return mediumLevelAttemptsNumber;
        } else if (level.equalsIgnoreCase("Heavy")) {
            return heavyLevelAttemptsNumber;
        } else {
            return 1;
        }
    }

    public static void play() {
        int levelAttemptsNumber = getLevelAttemptsNumber();
        int randomNumber = ThreadLocalRandom.current().nextInt(minRandomNumber, maxRandomNumber + 1);
        int userAttemptsCount = 0;
        System.out.println("Enter your number:");
        int userNumber = scanner.nextInt();
        userAttemptsCount++;
        while (userNumber != randomNumber) {
            if (userAttemptsCount < levelAttemptsNumber) {
                System.out.printf("Incorrect. Remaining attempts: %d. Try again:\n", levelAttemptsNumber - userAttemptsCount);
                userNumber = scanner.nextInt();
                userAttemptsCount++;
            } else {
                System.out.println("Game over.");
                break;
            }
        }
        if (userNumber == randomNumber) {
            System.out.println("Congratulations! You are the winner.");
        }
    }
}
